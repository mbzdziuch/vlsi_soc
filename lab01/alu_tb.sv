/*
M.Bzdziuch mtm_alu TestBench
*/

//ERROR FLAGS
`define F_CARRY 4'b1000
`define F_OVERFLOW 4'b0100
`define F_ZERO 4'b0010
`define F_NEGATIVE 4'b0001
`define F_DATA_ERR 6'b100100
`define F_CRC_ERR 6'b010010
`define F_OP_ERR 6'b001001

module top_mtmAlutb;
 
 
//------------------------------------------------------------------------------
// type and variable definitions
//------------------------------------------------------------------------------

   typedef enum bit[2:0] {and_op = 3'b000, 
                          or_op  = 3'b001,
                          add_op = 3'b100,
                          sub_op = 3'b101} operation_t;
	
	typedef enum bit[2:0] {and_state = 3'b000,
		or_state                     = 3'b001,
		add_state                    = 3'b100,
		sub_state                    = 3'b101,
		rst_state                    = 3'b010} state_t;
	
	bit 		sin;
   	bit			clk;
   	bit			rst_n;
	wire		sout;
	
	bit	[31:0]	B;
	bit [31:0]	A;
		
	state_t state;
   	operation_t  op_set;
	
	
	 

//------------------------------------------------------------------------------
// DUT instantiation
//------------------------------------------------------------------------------
 
   
   mtm_Alu DUT (.clk, .rst_n, .sin, .sout);
	 
// CRC generation functions

//CRC in generator
function [3:0] get_CRCin;
	input bit [67:0] datain;
    bit [67:0] d;
    bit [3:0] c;
    bit [3:0] newcrc;
		begin
    	d = datain;
    	c = 4'b0000;
    	newcrc[0] = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
    	newcrc[1] = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
    	newcrc[2] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
    	newcrc[3] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];	
		get_CRCin = newcrc;
		end
endfunction
  
//CRC out generator
function [2:0] get_CRCout;
	input bit [36:0] dataout;
    bit [36:0] d;
    bit [2:0] c;
    bit [2:0] newcrc;
		begin
    	d = dataout;
    	c = 3'b000;
    	newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
    	newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
    	newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
		get_CRCout = newcrc;
		end
endfunction

bit	[10:0]	input_frame;
bit	[3:0]	frame_counter;
bit	[98:0]	des_input_data;
bit	sin_des_done;

always@(negedge sin or negedge rst_n) begin : des_sin
	sin_des_done = 0;
	fork: recive_frame
		begin
			repeat(11) begin
				@(posedge clk);
				input_frame <<= 1;
				input_frame [0] = sin;
			end
			disable recive_frame;
		end
		wait(!rst_n) disable recive_frame;
		@(negedge rst_n) disable recive_frame;
	join
	des_input_data <<= 11;
	des_input_data[10:0] = input_frame;
	frame_counter++;
	if(!rst_n || frame_counter == 9 || input_frame[10:9] == 2'b01) begin
		frame_counter = 0;
		input_frame = 11'b0;
		if(rst_n)begin
			sin_des_done = 1;
		end
	end
	
end :des_sin
			
//------------------------------------------------------------------------------
// Coverage block
//------------------------------------------------------------------------------

//Process deserialized sin input and rst_n for coverage purposes
	bit [98:0] coverage_sin_data;
	state_t op_state;
	bit [31:0] A_cov;
	bit [31:0] B_cov;
	
	always @(posedge sin_des_done or negedge rst_n)begin
		coverage_sin_data = des_input_data;
		if(~rst_n)begin
			op_state = rst_state;
		end
		else begin
			B_cov = {coverage_sin_data[96-:8],coverage_sin_data[85-:8],coverage_sin_data[74-:8],coverage_sin_data[63-:8]};
			A_cov = {coverage_sin_data[52-:8],coverage_sin_data[41-:8],coverage_sin_data[30-:8],coverage_sin_data[19-:8]};
			
			case(operation_t'(coverage_sin_data[7-:3]))
				add_op: op_state = add_state;
				sub_op: op_state = sub_state;
				or_op:  op_state = or_state;
				and_state: op_state = and_state;
			endcase
		end
	end

//------------------------------------------------------------------------------
// Coverage block
//------------------------------------------------------------------------------
	
 covergroup op_cov;

      option.name = "cg_op_cov";

      coverpoint op_state {
         // #A1 test all operations
         bins A1_single_op[] = {[and_state : rst_state]};
      
         // #A2 test all operations after reset
         bins A2_rst_opn[] = (rst_state => [and_state:sub_state]);
         
         // #A3 test reset after all operations
         bins A3_opn_rst[] = ([and_state:sub_state] => rst_state);
      
         // #A4 Test each op after each
         bins A4_opn_opn[] = ([and_state:rst_state]=>[and_state:rst_state]);
         
         // #A5 two operations in row
         bins A5_twoops[] = ([and_state:rst_state] [* 2]);
      }

 endgroup
 
 covergroup zeros_ones_max_min_on_ops;

      option.name = "cg_zeros_ones_max_min_on_ops";

      all_ops : coverpoint op_state {
         ignore_bins null_ops = {rst_state};
      }

      a_leg: coverpoint A_cov {
         bins zeros = {'h00000000};
         bins max = {'h7FFFFFFF};
         bins min = {'h80000000};
         bins positive = {['h01:'h7FFFFFFE]};
	     bins negative = {['h80000001:'hFFFFFFFE]};
         bins ones  = {'hFFFFFFFF};
      }

      b_leg: coverpoint B_cov {
         bins zeros = {'h00000000};
         bins max = {'h7FFFFFFF};
         bins min = {'h80000000};
         bins positive = {['h01:'h7FFFFFFE]};
	     bins negative = {['h80000001:'hFFFFFFFE]};
         bins ones  = {'hFFFFFFFF};
      }

      B_op_00_FF_min_max:  cross a_leg, b_leg, all_ops {

         // #B1 simulate all zero input for all the operations

         bins B1_add_00 = binsof (all_ops) intersect {add_state} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         bins B1_and_00 = binsof (all_ops) intersect {and_state} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         bins B1_or_00 = binsof (all_ops) intersect {or_state} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         bins B1_sub_00 = binsof (all_ops) intersect {sub_state} &&
                       (binsof (a_leg.zeros) || binsof (b_leg.zeros));

         // #B2 simulate all one input for all the operations

         bins B2_add_FF = binsof (all_ops) intersect {add_state} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B2_and_FF = binsof (all_ops) intersect {and_state} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B2_or_FF = binsof (all_ops) intersect {or_state} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B2_sub_FF = binsof (all_ops) intersect {sub_state} &&
                       (binsof (a_leg.ones) || binsof (b_leg.ones));

         bins B3_add_ovf = binsof (all_ops) intersect {add_state} &&
                        (binsof (a_leg.max) && binsof (b_leg.positive));
         bins B4_sub_ovf = binsof (all_ops) intersect {sub_state} &&
                        (binsof (a_leg.positive) && binsof (b_leg.min));
	      
		ignore_bins positive_only =
                                  binsof(a_leg.positive) && binsof(b_leg.positive);
		ignore_bins negative_only =
                                  binsof(a_leg.negative) && binsof(b_leg.negative);
		ignore_bins positive_negative =
		                          binsof(a_leg.positive) && binsof(b_leg.negative);
		ignore_bins negative_positive =
		                          binsof(a_leg.negative) && binsof(b_leg.positive);
      }

   endgroup 
 
	op_cov oc;
 	zeros_ones_max_min_on_ops c_00_FF_min_max;
 
    initial begin : coverage
   
      oc = new();
	  c_00_FF_min_max = new();
   
      forever begin : sample_cov
		@(negedge sin_des_done or posedge rst_n)
        oc.sample();
	  	c_00_FF_min_max.sample();
      end
   end : coverage

//------------------------------------------------------------------------------
// Clock generator
//------------------------------------------------------------------------------

   initial begin : clk_gen
      clk = 0;
      forever begin : clk_frv
         #10;
         clk = ~clk;
      end
   end


//---------------------------------  
   function [31:0] get_data();
		bit [3:0] zero_ones;
		zero_ones = $random;
		if (zero_ones == 4'h0)
			return 32'h00000000;	//0
		else if (zero_ones == 4'hF)
			return 32'hFFFFFFFF;	//-1
		else if (zero_ones == 4'h1)
			return 32'h7FFFFFFF; 	//largest positive
		else if (zero_ones == 4'hE)
			return 32'h80000000; 	//largest negative
		else
			return $random;
	endfunction : get_data

function state_t get_state();
	bit [2:0] sel_state;
	sel_state = $random;
	case(sel_state)
		3'b000 :	return	and_state;
		3'b001 :	return	or_state;
		3'b010 :	return	add_state;
		3'b011 :	return	sub_state;
		3'b100 :	return	and_state;
		3'b101 :	return	or_state;	
		3'b110 :	return	rst_state;
		3'b111 :	return	rst_state;
	endcase
endfunction : get_state

   task send_frame;
	   input bit	[7:0]	din;
	   input bit	frame_sel;
	   bit 	[10:0]	frame_tx;
	   begin
		   frame_tx = {1'b0, frame_sel, din, 1'b1};
		   repeat(11)	begin
			   @(negedge clk);
			   sin = frame_tx[10];
			   frame_tx <<= 1;
		   end
	   end
   endtask
   
   	task send_package;
	   	input bit 	[31:0]	A;
	   	input bit 	[31:0]	B;
	   	input operation_t op;
	   	int i; 
	   	bit			[3:0] CRCin;
	   	
	   	begin
		  		CRCin = get_CRCin({B,A,1'b1,op});
			   	send_frame(B[31:24],0);
			   	send_frame(B[23:16],0);
			   	send_frame(B[15:8],0);
			   	send_frame(B[7:0],0);
			   	send_frame(A[31:24],0);
			   	send_frame(A[23:16],0);
			   	send_frame(A[15:8],0);
			   	send_frame(A[7:0],0);
			    send_frame({1'b0,op,CRCin},1);
	   	end
   	endtask
   	
			   	
//------------------------------------------------------------------------------
// Tester
//------------------------------------------------------------------------------
bit error = 0;
// Tester main
   
   initial begin : tester	  
      rst_n = 1'b0;	 
	  sin = 1;   
	  @(negedge clk);
	  @(negedge clk);
      rst_n = 1'b1;	 
      repeat (10000) begin : tester_main	      
         @(negedge clk);
		  state = get_state();
	      A = get_data();
	      B = get_data();
	      case(state)
		      rst_state:	begin	
			      @(negedge clk);
			      rst_n = 1'b0;
			      sin = 1;
		      end	
		      default:	begin
			      @(negedge clk);
			      rst_n = 1'b1;
			      case(state)
				      and_state:	op_set = and_op;
				      or_state:		op_set = or_op;
				      add_state:	op_set = add_op;
				      sub_state:	op_set = sub_op;
			      endcase
			      send_package(A,B,op_set);
		      end
	      endcase
      end	
      if(error) $display("Test: FAILED");
      else $display("Test: PASSED");
      $finish;
   end : tester
   
    
	     
		     
	    

//------------------------------------------------------------------------------
// Scoreboard
//------------------------------------------------------------------------------
	
	//Flag bits
	bit [3:0]	sinCRC;
   	bit [3:0]	expCRC;
   	bit [2:0]	expCRCout;
   	bit [2:0]	sinOP;
	bit	[5:0]	errF;
   	bit	[3:0]	expF;
	bit	carry;
   	bit overflow;
    bit zero;
    bit negative;
   	bit expar;
    bit [31:0]	inA;
   	bit [31:0]	inB;
   	bit	[98:0]	sbin_data;
    bit [10:0]	sbin_frame;
    bit [4:0] frameA_cnt;
   	bit [4:0] frameB_cnt;
   
   typedef struct packed
   {
	   bit	[31:0]	A;
	   bit	[31:0]	B;
	   bit	[4:0]	wA;
	   bit	[4:0]	wB;
	   bit	[2:0]	op;
	   bit	[4:0]	expw;
	   bit	[31:0]	expd;
	   bit	[7:0]	expctl;
   }	que_sin_t;
   que_sin_t que_sin [$];
   que_sin_t que_sin_el;
   
always@(posedge sin_des_done) begin : calc_predicted
	sbin_data = des_input_data;
	carry = 0;
	overflow = 0;
	zero = 0;
	negative = 0;
	frameA_cnt = 0;
	frameB_cnt = 0;
	begin: calc_data
		repeat(4)begin
			sbin_frame = sbin_data[98-:11];
			sbin_data <<= 11;
			if(sbin_frame[10:9] == 2'b00 && sbin_frame[0] == 1'b1)begin
				inB <<= 8;
				inB[7:0] = sbin_frame[8:1];
			end
			else begin
				errF = `F_DATA_ERR;
				disable calc_data;
			end
			frameB_cnt++;
		end
		repeat(4)begin
			sbin_frame = sbin_data[98-:11];
			sbin_data <<= 11;
			if(sbin_frame[10:9] == 2'b00 && sbin_frame[0] == 1'b1)begin
				inA <<= 8;
				inA[7:0] = sbin_frame[8:1];
			end
			else begin
				errF = `F_DATA_ERR;
				disable calc_data;
			end
			frameA_cnt++;
		end
		sbin_frame = sbin_data[98-:11];
		sbin_data <<= 11;
		if(sbin_frame[10:8] == 3'b010 && sbin_frame[0] == 1'b1)begin
				sinCRC = sbin_frame[4-:4];
				sinOP  = sbin_frame[7-:3];
		end
		else begin
				errF = `F_DATA_ERR;
				disable calc_data;
		end
		expCRC = get_CRCin({inB,inA,1'b1,sinOP});
		if(expCRC != sinCRC)begin
			errF = `F_CRC_ERR;
			disable calc_data;
		end
	end : calc_data
	que_sin_el.wB = frameB_cnt;
	que_sin_el.B       = inB;
	que_sin_el.wA = frameA_cnt;
	que_sin_el.A       = inA;
	que_sin_el.op      = sinOP;	
	
	if(errF == 6'b0)begin
			case(sinOP)
				add_op: begin
					{carry,que_sin_el.expd} = inA+inB;
					overflow                                 = (inA[31] & inB[31] & ~que_sin_el.expd[31])|(~inA[31] & ~inB[31] & que_sin_el.expd[31]);
				end
				sub_op: begin
					{carry,que_sin_el.expd}  = inB-inA;
					overflow                                 = (inB[31] & ~inA[31] & ~que_sin_el.expd[31])|(~inB[31] & inA[31] & que_sin_el.expd[31]);
				end
				and_op: begin
					que_sin_el.expd         = inA&inB;
				end
				or_op: begin
					que_sin_el.expd         = inA|inB;
				end
				default: errF                = `F_OP_ERR;
			endcase
			zero                             = ~(|que_sin_el.expd);
			negative                         = que_sin_el.expd[31];
		end
		if(errF == 6'b0)begin
			que_sin_el.expw = 5;
			expF                   = {carry,overflow,zero,negative};
			expCRCout                 = get_CRCout({que_sin_el.expd,1'b0, expF});
			que_sin_el.expctl   = {1'b0,expF,expCRCout};
		end
		else begin
			que_sin_el.expw = 1;
			expar                  = 1'b1^(^errF);
			que_sin_el.expctl   = {1'b1,expF,expar};
		end
		
		que_sin.push_front(que_sin_el);
end : calc_predicted
	
	bit	[10:0]	frame_out;
	bit [4:0]	outFcnt;
	bit [54:0]	des_sout;
	bit sout_des_done;
	que_sin_t que_sout_el;
	que_sin_t dcomp;
	
	always@(negedge sout) begin : out_des
		sout_des_done = 0;
		fork: recive_frame
			begin
				repeat(11)begin
					@(posedge clk);
					frame_out<<=1;
					frame_out[0] = sout;
				end
				disable recive_frame;
			end
			@(negedge rst_n) disable recive_frame;
		join
		des_sout[(5-outFcnt)*11-1-:11] = frame_out;
		outFcnt++;
		if(!rst_n || outFcnt == 5 || frame_out[10:9] == 2'b01)begin
			outFcnt =0;
			frame_out = 11'b0;
			if(rst_n)begin
				sout_des_done = 1;
			end
		end
	end: out_des
	
	always @(negedge rst_n)begin
			que_sin.pop_front();
	end
	
	bit	[54:0]	sboutd;
	bit [4:0]	outDcnt;
	always@(posedge sout_des_done)begin : check
		sboutd = des_sout;
		outDcnt = 0;
		repeat(5)begin : conc_sbout;
			#1;
			frame_out = sboutd[54-:11];
			sboutd <<=11;
			outDcnt++;
			if(frame_out[10:9] == 2'b01 && frame_out[0] == 1'b1)begin
				que_sout_el.expctl= frame_out[8:1];
				disable conc_sbout;
			end
			else if(frame_out[10:9] == 2'b00 && frame_out[0] == 1'b1)begin	
				que_sout_el.expd<<=8;
				que_sout_el.expd[7:0]= frame_out[8:1];
			end
			else begin
				$error("Test Failed - wrong frame: %b. Alu frame format should follow: 0[0/1]bbbbbbbb1.", frame_out);
				error = 1;
				disable conc_sbout;
			end
		end : conc_sbout
		dcomp = que_sin.pop_back();
		if(outDcnt == 1 & dcomp.expw == 1)begin
			if(dcomp.expctl!==que_sout_el.expctl)begin
				$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n", dcomp.A, dcomp.B,dcomp.op,que_sout_el.expctl);
				$display("It should be:%h", dcomp.expctl);
				error = 1;
			end
		end
		else if( outDcnt == 5 && dcomp.expw == 5)begin
			if(dcomp.expd!=que_sout_el.expd)begin
				$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n. Result FAILED", dcomp.A, dcomp.B,dcomp.op,que_sout_el.expd);
				$display("It should be:%h", dcomp.expd);
				error = 1;
			end
			else if(dcomp.expctl != que_sout_el.expctl)begin
				$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n", dcomp.A, dcomp.B,dcomp.op,que_sout_el.expctl);
				$display("It should be:%h", dcomp.expctl);
				error = 1;
			end
		end
		else begin
			$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n Frame width FAILED.Should be 1-5bytes Received: %d", dcomp.A, dcomp.B,dcomp.op, que_sout_el.expd, outDcnt);
			$display(dcomp.expw);
			error = 1;
		end		
	end : check							   
endmodule : top_mtmAlutb
