package alu_pkg;
	
	typedef enum bit[2:0] {and_op = 3'b000, 
                           or_op  = 3'b001,
                           add_op = 3'b100,
                           sub_op = 3'b101} operation_t;
	
	typedef enum bit[2:0] {and_state = 3'b000,
		or_state                     = 3'b001,
		add_state                    = 3'b100,
		sub_state                    = 3'b101,
		rst_state                    = 3'b010} state_t;
		
	typedef struct packed
   {
	   bit	[31:0]	A;
	   bit	[31:0]	B;
	   bit	[4:0]	wA;
	   bit	[4:0]	wB;
	   bit	[2:0]	op;
	   bit	[4:0]	expw;
	   bit	[31:0]	expd;
	   bit	[7:0]	expctl;
   }	que_sin_t;
	
`include "coverage.svh"
`include "tester.svh"
`include "scoreboard.svh"
`include "testbench.svh"


endpackage : alu_pkg
   