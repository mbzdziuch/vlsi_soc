/*
scoreboard module file
*/

`define F_DATA_ERR 6'b100100
`define F_CRC_ERR 6'b010010
`define F_OP_ERR 6'b001001

class scoreboard;
	virtual alu_bfm bfm;
	
	function new(virtual alu_bfm b);
		bfm =b;
	endfunction : new

//CRC out generator
protected function [2:0] get_CRCout;
	input bit [36:0] dataout;
    bit [36:0] d;
    bit [2:0] c;
    bit [2:0] newcrc;
		begin
    	d = dataout;
    	c = 3'b000;
    	newcrc[0] = d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1];
    	newcrc[1] = d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2];
    	newcrc[2] = d[36] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[2];
		get_CRCout = newcrc;
		end
endfunction




   //------------------------------------------------------------------------------
// Scoreboard
//------------------------------------------------------------------------------
	
	//Flag bits
	bit [3:0]	sinCRC;
   	bit [3:0]	expCRC;
   	bit [2:0]	expCRCout;
   	bit [2:0]	sinOP;
	bit	[5:0]	errF;
   	bit	[3:0]	expF;
	bit	carry;
   	bit overflow;
    bit zero;
    bit negative;
   	bit expar;
    bit [31:0]	inA;
   	bit [31:0]	inB;
   	bit	[98:0]	sbin_data;
    bit [10:0]	sbin_frame;
    bit [4:0] frameA_cnt;
   	bit [4:0] frameB_cnt;
   
   que_sin_t que_sin [$];
   que_sin_t que_sin_el;
   
   task calc_predicted();
	forever begin 
	@(posedge bfm.sin_des_done);
	sbin_data = bfm.des_input_data;
	carry = 0;
	overflow = 0;
	zero = 0;
	negative = 0;
	frameA_cnt = 0;
	frameB_cnt = 0;
	begin: calc_data
		repeat(4)begin
			sbin_frame = sbin_data[98-:11];
			sbin_data <<= 11;
			if(sbin_frame[10:9] == 2'b00 && sbin_frame[0] == 1'b1)begin
				inB <<= 8;
				inB[7:0] = sbin_frame[8:1];
			end
			else begin
				errF = `F_DATA_ERR;
				disable calc_data;
			end
			frameB_cnt++;
		end
		repeat(4)begin
			sbin_frame = sbin_data[98-:11];
			sbin_data <<= 11;
			if(sbin_frame[10:9] == 2'b00 && sbin_frame[0] == 1'b1)begin
				inA <<= 8;
				inA[7:0] = sbin_frame[8:1];
			end
			else begin
				errF = `F_DATA_ERR;
				disable calc_data;
			end
			frameA_cnt++;
		end
		sbin_frame = sbin_data[98-:11];
		sbin_data <<= 11;
		if(sbin_frame[10:8] == 3'b010 && sbin_frame[0] == 1'b1)begin
				sinCRC = sbin_frame[4-:4];
				sinOP  = sbin_frame[7-:3];
		end
		else begin
				errF = `F_DATA_ERR;
				disable calc_data;
		end
		expCRC = bfm.get_CRCin({inB,inA,1'b1,sinOP});
		if(expCRC != sinCRC)begin
			errF = `F_CRC_ERR;
			disable calc_data;
		end
	end : calc_data
	que_sin_el.wB = frameB_cnt;
	que_sin_el.B       = inB;
	que_sin_el.wA = frameA_cnt;
	que_sin_el.A       = inA;
	que_sin_el.op      = sinOP;	
	
	if(errF == 6'b0)begin
			case(sinOP)
				add_op: begin
					{carry,que_sin_el.expd} = inA+inB;
					overflow                                 = (inA[31] & inB[31] & ~que_sin_el.expd[31])|(~inA[31] & ~inB[31] & que_sin_el.expd[31]);
				end
				sub_op: begin
					{carry,que_sin_el.expd}  = inB-inA;
					overflow                                 = (inB[31] & ~inA[31] & ~que_sin_el.expd[31])|(~inB[31] & inA[31] & que_sin_el.expd[31]);
				end
				and_op: begin
					que_sin_el.expd         = inA&inB;
				end
				or_op: begin
					que_sin_el.expd         = inA|inB;
				end
				default: errF                = `F_OP_ERR;
			endcase
			zero                             = ~(|que_sin_el.expd);
			negative                         = que_sin_el.expd[31];
		end
		if(errF == 6'b0)begin
			que_sin_el.expw = 5;
			expF                   = {carry,overflow,zero,negative};
			expCRCout                 = get_CRCout({que_sin_el.expd,1'b0, expF});
			que_sin_el.expctl   = {1'b0,expF,expCRCout};
		end
		else begin
			que_sin_el.expw = 1;
			expar                  = 1'b1^(^errF);
			que_sin_el.expctl   = {1'b1,expF,expar};
		end
		
		que_sin.push_front(que_sin_el);
		end
endtask 
	
	bit	[10:0]	frame_out;
	bit [4:0]	outFcnt;
	bit [54:0]	des_sout;
	bit sout_des_done;
	que_sin_t que_sout_el;
	que_sin_t dcomp;
	task out_des();
		forever begin 
		@(negedge bfm.sout); 
		sout_des_done = 0;
		fork: recive_frame
			begin
				repeat(11)begin
					@(posedge bfm.clk);
					frame_out<<=1;
					frame_out[0] = bfm.sout;
				end
				disable recive_frame;
			end
			@(negedge bfm.rst_n) disable recive_frame;
		join
		des_sout[(5-outFcnt)*11-1-:11] = frame_out;
		outFcnt++;
		if(!bfm.rst_n || outFcnt == 5 || frame_out[10:9] == 2'b01)begin
			outFcnt =0;
			frame_out = 11'b0;
			if(bfm.rst_n)begin
				sout_des_done = 1;
			end
		end
		end
	endtask
	
	task clear_que();
		forever begin
			@(negedge bfm.rst_n)
			que_sin.pop_front();
			end
	endtask
	
	bit	[54:0]	sboutd;
	bit [4:0]	outDcnt;
	task check();
		forever begin
		@(posedge sout_des_done)
		sboutd = des_sout;
		outDcnt = 0;
		repeat(5)begin : conc_sbout;
			#1;
			frame_out = sboutd[54-:11];
			sboutd <<=11;
			outDcnt++;
			if(frame_out[10:9] == 2'b01 && frame_out[0] == 1'b1)begin
				que_sout_el.expctl= frame_out[8:1];
				disable conc_sbout;
			end
			else if(frame_out[10:9] == 2'b00 && frame_out[0] == 1'b1)begin	
				que_sout_el.expd<<=8;
				que_sout_el.expd[7:0]= frame_out[8:1];
			end
			else begin
				$error("Test Failed - wrong frame: %b. Alu frame format should follow: 0[0/1]bbbbbbbb1.", frame_out);
				disable conc_sbout;
			end
		end : conc_sbout
		dcomp = que_sin.pop_back();
		if(outDcnt == 1 & dcomp.expw == 1)begin
			if(dcomp.expctl!==que_sout_el.expctl)begin
				$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n", dcomp.A, dcomp.B,dcomp.op,que_sout_el.expctl);
				$display("It should be:%h", dcomp.expctl);
			end
		end
		else if( outDcnt == 5 && dcomp.expw == 5)begin
			if(dcomp.expd!=que_sout_el.expd)begin
				$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n. Result FAILED", dcomp.A, dcomp.B,dcomp.op,que_sout_el.expd);
				$display("It should be:%h", dcomp.expd);
			end
			else if(dcomp.expctl != que_sout_el.expctl)begin
				$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n", dcomp.A, dcomp.B,dcomp.op,que_sout_el.expctl);
				$display("It should be:%h", dcomp.expctl);
			end
		end
		else begin
			$error ("FAILED: A: %h, B: %h, op: %b, result %h.\n Frame width FAILED.Should be 1-5bytes Received: %d", dcomp.A, dcomp.B,dcomp.op, que_sout_el.expd, outDcnt);
			$display(dcomp.expw);
		end	
		end
	endtask : check		
	task execute();
		fork
			check;
			calc_predicted;
			clear_que;
			out_des;
		join
	endtask
	
endclass 