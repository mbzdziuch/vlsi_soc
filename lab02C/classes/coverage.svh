/*
coverage module file
 */
class coverage;
	virtual alu_bfm bfm;


//------------------------------------------------------------------------------
// Coverage block
//------------------------------------------------------------------------------
	bit [98:0] cov_sin_data;
	state_t op_state;
	bit [31:0] Ac;
	bit [31:0] Bc;
	
	task covdes();
		forever begin
		@(posedge bfm.sin_des_done or negedge bfm.rst_n);
		cov_sin_data = bfm.des_input_data;
		if(~bfm.rst_n)begin
			op_state = rst_state;
		end
		else begin
			Bc = {cov_sin_data[96-:8],cov_sin_data[85-:8],cov_sin_data[74-:8],cov_sin_data[63-:8]};
			Ac = {cov_sin_data[52-:8],cov_sin_data[41-:8],cov_sin_data[30-:8],cov_sin_data[19-:8]};
			
			case(operation_t'(cov_sin_data[7-:3]))
				add_op: op_state = add_state;
				sub_op: op_state = sub_state;
				or_op:  op_state = or_state;
				and_state: op_state = and_state;
			endcase
		end
		end
	endtask
	
 covergroup op_cov;
      option.name = "cg_op_cov";
      coverpoint op_state {
         bins A1_single_op[] 	= {[and_state : rst_state]};
         bins A2_rst_opn[] 		= (rst_state => [and_state:sub_state]);
         bins A3_opn_rst[] 		= ([and_state:sub_state] => rst_state);
         bins A4_opn_opn[] 		= ([and_state:rst_state]=>[and_state:rst_state]);
         bins A5_twoops[] 		= ([and_state:rst_state] [* 2]);
      }
 endgroup
 
 covergroup zeros_ones_max_min_on_ops;
      option.name = "cg_zeros_ones_max_min_on_ops";
      all_ops : coverpoint op_state {
         ignore_bins null_ops = {rst_state};
      }
      a_leg: coverpoint Ac {
         bins zeros = {'h00000000};
         bins max = {'h7FFFFFFF};
         bins min = {'h80000000};
         bins positive = {['h01:'h7FFFFFFE]};
	     bins negative = {['h80000001:'hFFFFFFFE]};
         bins ones  = {'hFFFFFFFF};
      }
      b_leg: coverpoint Bc {
         bins zeros = {'h00000000};
         bins max = {'h7FFFFFFF};
         bins min = {'h80000000};
         bins positive = {['h01:'h7FFFFFFE]};
	     bins negative = {['h80000001:'hFFFFFFFE]};
         bins ones  = {'hFFFFFFFF};
      }
      B_op_00_FF_min_max:  cross a_leg, b_leg, all_ops {
         bins B1_add_00 = binsof (all_ops) intersect {add_state} && (binsof (a_leg.zeros) || binsof (b_leg.zeros));
         bins B1_and_00 = binsof (all_ops) intersect {and_state} && (binsof (a_leg.zeros) || binsof (b_leg.zeros));
         bins B1_or_00 =  binsof (all_ops) intersect {or_state}  && (binsof (a_leg.zeros) || binsof (b_leg.zeros));
         bins B1_sub_00 = binsof (all_ops) intersect {sub_state} && (binsof (a_leg.zeros) || binsof (b_leg.zeros));
         bins B2_add_FF = binsof (all_ops) intersect {add_state} && (binsof (a_leg.ones)  || binsof (b_leg.ones));
         bins B2_and_FF = binsof (all_ops) intersect {and_state} && (binsof (a_leg.ones) || binsof (b_leg.ones));
         bins B2_or_FF = binsof (all_ops)  intersect {or_state}  && (binsof (a_leg.ones) || binsof (b_leg.ones));
         bins B2_sub_FF = binsof (all_ops) intersect {sub_state} && (binsof (a_leg.ones) || binsof (b_leg.ones));
         bins B3_add_ovf = binsof (all_ops) intersect {add_state} && (binsof (a_leg.max) && binsof (b_leg.positive));
         bins B4_sub_ovf = binsof (all_ops) intersect {sub_state} && (binsof (a_leg.positive) && binsof (b_leg.min));
	      
		ignore_bins positive_only 		= binsof(a_leg.positive) && binsof(b_leg.positive);
		ignore_bins negative_only 		= binsof(a_leg.negative) && binsof(b_leg.negative);
		ignore_bins positive_negative 	= binsof(a_leg.positive) && binsof(b_leg.negative);
		ignore_bins negative_positive 	= binsof(a_leg.negative) && binsof(b_leg.positive);
      }
 endgroup  
 
 	function new(virtual alu_bfm b);	 	 
		op_cov 						=new();
 		zeros_ones_max_min_on_ops 	=new();
		bfm							=b;
 	endfunction : new
 
 	task execute();
	 	fork
		covdes;
		forever begin 
		@(negedge bfm.sin_des_done or posedge bfm.rst_n);
     	op_cov.sample();
	 	zeros_ones_max_min_on_ops.sample();
		end
		join
 	endtask
 
endclass : coverage





