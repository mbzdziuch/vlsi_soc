/*
tester module file
*/
class tester;
	virtual alu_bfm bfm;
	
	function new(virtual alu_bfm b);
		bfm=b;
	endfunction : new
		
	protected function state_t get_state();
	bit [2:0] sel_state;
	sel_state = $random;
	case(sel_state)
		3'b000 :	return	and_state;
		3'b001 :	return	or_state;
		3'b010 :	return	add_state;
		3'b011 :	return	sub_state;
		3'b100 :	return	and_state;
		3'b101 :	return	or_state;	
		3'b110 :	return	rst_state;
		3'b111 :	return	rst_state;
	endcase
endfunction : get_state
	

	
  	protected function [31:0] get_data();
		bit [3:0] zero_ones;
		zero_ones = $random;
		if (zero_ones == 4'h0)
			return 32'h00000000;
		else if (zero_ones == 4'hF)
			return 32'hFFFFFFFF;	
		else if (zero_ones == 4'h1)
			return 32'h7FFFFFFF; 	
		else if (zero_ones == 4'hE)
			return 32'h80000000;
		else
			return $random;
  	endfunction : get_data
  	
task execute();
    bit	[31:0]	A;
  	bit	[31:0]	B;	
	state_t state;
	operation_t  op_set;
	   bfm.reset_alu();	 
      repeat (10000) begin : tester_main	      
         @(negedge bfm.clk);
		  state = get_state();
	      A = get_data();
	      B = get_data();
	      case(state)
		      rst_state:	begin	
			      @(negedge bfm.clk);			      
			      bfm.rst_n = 1'b0;
			      bfm.sin = 1;
		      end	
		      default:	begin
			      @(negedge bfm.clk);
			      bfm.rst_n = 1'b1;
			      case(state)
				      and_state:	op_set = and_op;
				      or_state:		op_set = or_op;
				      add_state:	op_set = add_op;
				      sub_state:	op_set = sub_op;
			      endcase
			      bfm.send_package(A,B,op_set);
		      end
	      endcase
      end	
      $finish;
   endtask 
endclass : tester