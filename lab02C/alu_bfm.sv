/*
BFM module
*/
interface alu_bfm;
   import alu_pkg::*;	
	
   bit          clk;
   bit          rst_n;
   bit			sin;
   bit          sout;
   
   initial begin
      clk = 0;
      forever begin
         #10;
         clk = ~clk;
      end
   end


   task reset_alu();
      rst_n = 1'b0;
	  sin = 1'b1;
      @(negedge clk);
      @(negedge clk);
      rst_n = 1'b1;      
   endtask : reset_alu
   
	bit	[10:0]	input_frame;
	bit	[3:0]	frame_counter;
	bit	[98:0]	des_input_data;
	bit	sin_des_done;
	always@(negedge sin or negedge rst_n) begin : des_sin
		sin_des_done = 0;
		fork: recive_frame
			begin
				repeat(11) begin
				@(posedge clk);
				input_frame <<= 1;
				input_frame [0] = sin;
				end
			disable recive_frame;
			end
		wait(!rst_n) disable recive_frame;
		@(negedge rst_n) disable recive_frame;
		join
		des_input_data <<= 11;
		des_input_data[10:0] = input_frame;
		frame_counter++;
		if(!rst_n || frame_counter == 9 || input_frame[10:9] == 2'b01) begin
			frame_counter = 0;
			input_frame = 11'b0;
			if(rst_n)begin
				sin_des_done = 1;
			end
		end	
	end :des_sin

	task send_frame;
	   input bit	[7:0]	din;
	   input bit	frame_sel;
	   bit 	[10:0]	frame_tx;
	   begin
		   frame_tx = {1'b0, frame_sel, din, 1'b1};
		   repeat(11)	begin
			   @(negedge clk);
			   sin = frame_tx[10];
			   frame_tx <<= 1;
		   end
	   end
	endtask



//CRC in generator
	function [3:0] get_CRCin;
		input bit [67:0] datain;
    	bit [67:0] d;
    	bit [3:0] c;
    	bit [3:0] newcrc;
			begin
    		d = datain;
    		c = 4'b0000;
    		newcrc[0] = d[66] ^ d[64] ^ d[63] ^ d[60] ^ d[56] ^ d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2];
    		newcrc[1] = d[67] ^ d[66] ^ d[65] ^ d[63] ^ d[61] ^ d[60] ^ d[57] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3];
    		newcrc[2] = d[67] ^ d[66] ^ d[64] ^ d[62] ^ d[61] ^ d[58] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[2] ^ c[3];
    		newcrc[3] = d[67] ^ d[65] ^ d[63] ^ d[62] ^ d[59] ^ d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[44] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[1] ^ c[3];	
			get_CRCin = newcrc;
			end
	endfunction

	task send_package;
	   	input bit 	[31:0]	A;
	   	input bit 	[31:0]	B;
	   	input operation_t op;
	   	int i; 
	   	bit	[3:0] CRCin;	   	
	   	begin
		  		CRCin = get_CRCin({B,A,1'b1,op});
			   	send_frame(B[31:24],0);
			   	send_frame(B[23:16],0);
			   	send_frame(B[15:8],0);
			   	send_frame(B[7:0],0);
			   	send_frame(A[31:24],0);
			   	send_frame(A[23:16],0);
			   	send_frame(A[15:8],0);
			   	send_frame(A[7:0],0);
			    send_frame({1'b0,op,CRCin},1);
	   	end
   	endtask
   
endinterface : alu_bfm