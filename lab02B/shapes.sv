/*
M.Bzdziuch lab02B shapes 
*/
 
 virtual class shape;	 
	 protected real width;
	 protected real height;	 
	 function new(real w, real h);
		 width = w;
		 height = h;
	 endfunction	 
	 pure virtual function real get_area();	 
	 pure virtual function void print();	 
 endclass 
 
 class rectangle extends shape;
	 	function new(real w, real h);
		 super.new(w,h);
	 endfunction
	 virtual function real get_area();
		 return super.width * super.height;
	 endfunction
	 function void print();
		$display ("Rectangle w=%g h=%g area=%g", super.width, super.height, get_area());
	 endfunction	 
 endclass
 
  class square extends shape;
	function new(real w, real h);
		  super.new(w,w);	 
	 endfunction
	 virtual function real get_area();
		 return width * height;
	 endfunction
	 function void print();
		$display ("Square w=%g area=%g", width, get_area());
	 endfunction
 endclass
 
   class triangle extends shape;
	 function new(real w, real h);
		 super.new(w,h);
	 endfunction
	 virtual function real get_area();
		 return width * (height * 0.5);
	 endfunction
	 function void print();
		$display ("Triangle w=%g h=%g area=%g", width,height , get_area);
	 endfunction
	 
   endclass
   
   class shape_factory;
	    static function shape make_shape(string shape_type, real w, real h);
		rectangle rectangle_h;
 		square square_h;
 		triangle triangle_h;
		    if (shape_type=="rectangle")begin
			    rectangle_h=new(w,h);
				return rectangle_h;
		    end
		    else if (shape_type=="square")begin
			    square_h=new(w,h);
				return square_h;
		    end
		    else if (shape_type=="triangle")begin
			    triangle_h=new(w,h);
				return triangle_h;
		    end
		    else begin 
			    $fatal (1, {"Incorrect shape: ", shape_type});
			    end
		    endfunction
   endclass : shape_factory
   
   class shape_reporter #(type T=shape);
	   protected static T shape_storage [$];
	   static function void store_shape(T s);
		   shape_storage.push_back(s);
	   endfunction
	   static function void report_shapes();
		   static real shapes_area = 0;
		   foreach (shape_storage[i]) begin
			   shapes_area += shape_storage[i].get_area();
			   shape_storage[i].print();
		   end
		   $display("Total area: %g\n", shapes_area);
	   endfunction
   endclass
	 	    
 
 module top_shapes;
 initial begin
 shape shape_h;
 rectangle rectangle_h;
 square square_h;
 triangle triangle_h;
	 string shape_type;
	 real w;
	 real h;
	 bit rectangle_c;
	 bit square_c;
	 bit triangle_c;	 
	 int fileid;
	 int l;
	 
	 fileid = $fopen("lab02B_shapes.txt", "r");
	 if(fileid==0) begin
		 $display("ERROR : CAN NOT OPEN THE INPUT FILE");
	 end
	 else begin
	 while (!$feof(fileid)) begin
		 l = $fscanf(fileid,"%s %g %g",shape_type,w,h);
		 if(l==3)begin
			 shape_h = shape_factory::make_shape(shape_type, w, h);			 
			 rectangle_c = $cast(rectangle_h, shape_h);
	 		 square_c = $cast(square_h, shape_h);
	         triangle_c = $cast(triangle_h, shape_h);			 
			 if (rectangle_c == 1) begin
				 shape_reporter#(rectangle)::store_shape(rectangle_h);
			 end
			 else if(square_c == 1)begin
				 shape_reporter#(square)::store_shape(square_h);
				 end
			 else if(triangle_c == 1)begin
				 shape_reporter#(triangle)::store_shape(triangle_h);
			 end
			 else begin
				 $fatal(1,"CASTING FAILED");
				 end				 
			end
	 end
	 shape_reporter#(rectangle)::report_shapes();
	 shape_reporter#(square)::report_shapes();
	 shape_reporter#(triangle)::report_shapes();
	 end	
	 end
endmodule : top_shapes
